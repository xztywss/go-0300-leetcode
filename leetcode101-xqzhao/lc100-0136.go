package leetcode101_xqzhao

// TODO 只出现一次的数字 【-】 位运算

func singleNumber(nums []int) int {
	sign := 0
	for _, v := range nums {
		sign = sign ^ v
	}
	return sign
}
