package leetcode101_xqzhao

// TODO 两数之和 【-】 哈希表

func twoSum(nums []int, target int) []int {
	n := len(nums)
	m := make(map[int]int)

	for i := 0; i < n; i++ {
		if v, ok := m[target-nums[i]]; ok {
			return []int{i, v}
		}
		m[nums[i]] = i
	}

	return []int{}
}
