package leetcode101_xqzhao

// TODO 爬楼梯 【-】 动态规划

func climbStairs(n int) int {
	if n == 1 {
		return 1
	}
	dp := make([]int, n)

	// 动态规划 - 设置初始值
	dp[0] = 1
	dp[1] = 2

	// 动态规划 - 状态转移
	for i := 2; i < n; i++ {
		dp[i] = dp[i-1] + dp[i-2]
	}

	return dp[n-1]
}
