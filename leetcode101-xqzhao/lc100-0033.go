package leetcode101_xqzhao

// TODO 【没思路】 搜索旋转排序数组 【-】 二分查找

func search(nums []int, target int) int {
	left, right := 0, len(nums)-1

	for left <= right {
		mid := (left + right) / 2
		if nums[mid] == target {
			return mid
		}

		if nums[mid] >= nums[left] {
			// mid 在左边
			if target < nums[mid] && target >= nums[left] {
				// traget 在 A段
				right = mid - 1
			} else {
				// target 在 B段 或 C段
				left = mid + 1
			}
		} else {
			// mid 在右边
			if target > nums[mid] && target <= nums[right] {
				// traget 在 B段
				left = mid + 1
			} else {
				// target 在 A段 或 C段
				right = mid - 1
			}
		}
	}
	return -1
}
