package leetcode101_xqzhao

// TODO 对称二叉树 【-】 递归

func isSymmetric(root *TreeNode) bool {
	sign := true

	var dfs101 func(left *TreeNode, right *TreeNode)
	dfs101 = func(left *TreeNode, right *TreeNode) {
		// 递归结束控制器
		if left == nil && right == nil {
			return
		} else if left == nil || right == nil || left.Val != right.Val {
			sign = false
			return
		}
		// 继续递归
		dfs101(left.Left, right.Right)
		dfs101(left.Right, right.Left)
	}

	dfs101(root.Left, root.Right)
	return sign
}
