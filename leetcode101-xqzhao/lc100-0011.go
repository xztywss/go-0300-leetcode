package leetcode101_xqzhao

// TODO 盛最多水的容器 【-】 脑筋急转弯

func maxArea(height []int) int {
	n := len(height)
	l, r := 0, n-1
	area, maxArea := 0, 0

	for l < r {
		area = min(height[l], height[r]) * (r - l)
		if area > maxArea {
			maxArea = area
		}
		if height[l] > height[r] {
			r--
		} else {
			l++
		}
	}

	return maxArea
}

func min(a int, b int) int {
	if a > b {
		return b
	}
	return a
}
