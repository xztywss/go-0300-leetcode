package leetcode101_xqzhao

// TODO 合并区间 【-】 脑筋急转弯

import "sort"

func merge(intervals [][]int) [][]int {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	n := len(intervals)
	res := make([][]int, 0)

	for i := 0; i < len(intervals)-1; i++ {
		if intervals[i][1] >= intervals[i+1][0] { // 代表空间有重合
			intervals[i+1][0] = min3(intervals[i][0], intervals[i+1][0])
			intervals[i+1][1] = max3(intervals[i][1], intervals[i+1][1])
		} else { // 空间没有重合
			res = append(res, intervals[i])
		}
	}
	res = append(res, intervals[n-1])

	return res
}

func min3(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

func max3(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
