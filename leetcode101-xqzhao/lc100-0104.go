package leetcode101_xqzhao

// TODO 二叉树的最大深度 【-】 递归

func maxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	maxDeep := 0

	var dfs104 func(node *TreeNode, deep int)
	dfs104 = func(node *TreeNode, deep int) {
		// 结束条件
		if node == nil {
			return
		}
		// 保存结果
		maxDeep = max4(maxDeep, deep)
		// 继续递归
		dfs104(node.Left, deep+1)
		dfs104(node.Right, deep+1)
	}
	dfs104(root, 1)
	return maxDeep
}

func max4(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
