package leetcode101_xqzhao

// TODO 【没思路】 乘积最大子数组 【-】 动态规划 [优化空间]

func maxProduct(nums []int) int {
	// 优化了 空间复杂度 的 动态规划
	preMin, preMax, maxRes := nums[0], nums[0], nums[0]
	for i := 1; i < len(nums); i++ {
		tmp1 := preMin * nums[i]
		tmp2 := preMax * nums[i]
		preMin = min7(min7(tmp1, tmp2), nums[i])
		preMax = max7(max7(tmp1, tmp2), nums[i])
		maxRes = max7(maxRes, preMax)
	}
	return maxRes
}

func min7(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max7(a, b int) int {
	if a > b {
		return a
	}
	return b
}
