package leetcode101_xqzhao

// TODO 子集 【-】 递归

func subsets(nums []int) [][]int {
	n := len(nums)
	res := make([][]int, 0)
	path := make([]int, 0)

	var dfs078 func(i int)
	dfs078 = func(i int) {

		// n==-1 时, 说明需要开启第一轮循环, 所以不保存数据
		if i != -1 {
			// 保存结果
			path = append(path, nums[i])
			tmpPath := make([]int, len(path))
			copy(tmpPath, path)
			res = append(res, tmpPath)
		}

		// 递归主体 | 同时也扮演着 控制递归结束 的角色
		for j := i + 1; j < n; j++ {
			dfs078(j)
			path = path[:len(path)-1]
		}
	}
	res = append(res, []int{})
	dfs078(-1)

	return res
}
