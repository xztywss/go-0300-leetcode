package leetcode101_xqzhao

// TODO 无重复字符的最长子串 【-】 滑动窗口

func lengthOfLongestSubstring(s string) int {
	n, l, r := len(s), 0, 0
	if n <= 1 {
		return n
	}
	m := make(map[byte]int)
	maxLen := 0

	for r < n {
		if _, ok := m[s[r]]; ok { // 如果 map 中已经存在该字符
			delete(m, s[l])
			l++
		} else { // map 中不存在该字符
			maxLen = max(maxLen, r-l+1)
			m[s[r]] = 1
			r++
		}
	}
	return maxLen
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
