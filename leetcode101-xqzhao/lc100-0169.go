package leetcode101_xqzhao

// TODO 多数元素 【-】 动态规划 | 优化空间复杂度

func majorityElement(nums []int) int {
	maxVal, maxNum := nums[0], 1

	for i := 1; i < len(nums); i++ {
		if nums[i] == maxVal {
			maxNum++
		} else {
			if maxNum != 0 {
				maxNum--
			} else {
				maxVal = nums[i]
				maxNum++
			}
		}
	}
	return maxVal
}
