package leetcode101_xqzhao

// TODO 二叉树的中序遍历 【-】 中序遍历 | 递归

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func inorderTraversal(root *TreeNode) []int {
	res := make([]int, 0)

	var dfs094 func(node *TreeNode)
	dfs094 = func(node *TreeNode) {
		// 递归终止条件
		if node == nil {
			return
		}

		// 保存结果 + 继续递归
		dfs094(node.Left)
		res = append(res, node.Val)
		dfs094(node.Right)
	}

	dfs094(root)
	return res
}
