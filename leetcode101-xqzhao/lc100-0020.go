package leetcode101_xqzhao

// TODO 有效的括号 【-】 栈

func isValid(s string) bool {
	n := len(s)
	if n%2 == 1 {
		return false
	}
	//stack := make([]byte, n)	// 这么创建的不是空切片, 不是栈
	stack := []byte{}
	zip := map[byte]byte{
		')': '(',
		']': '[',
		'}': '{',
	}

	for i := 0; i < n; i++ {
		if s[i] == '(' || s[i] == '[' || s[i] == '{' {
			stack = append(stack, s[i])
		} else {
			m := len(stack)
			if m == 0 || zip[s[i]] != stack[m-1] {
				return false
			}
			stack = stack[:m-1]
		}
	}
	if len(stack) == 0 {
		return true
	}
	return false
}
