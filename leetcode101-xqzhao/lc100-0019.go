package leetcode101_xqzhao

// TODO 删除链表的倒数第 N 个结点 【-】 链表

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	length := getLength(head)
	if length == 1 || n == length {
		head = head.Next
		return head
	}
	prev := head
	for k := 1; k < length-n; k++ {
		prev = prev.Next
	}
	prev.Next = prev.Next.Next

	return head
}

func getLength(head *ListNode) int {
	len := 0
	for head != nil {
		len++
		head = head.Next
	}
	return len
}
