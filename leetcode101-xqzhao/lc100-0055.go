package leetcode101_xqzhao

// TODO 跳跃游戏 【-】 贪心算法

func canJump(nums []int) bool {
	n := len(nums)
	maxTep := 0
	if n == 1 {
		return true
	}

	for i := 0; i < n; i++ {
		// 如果 nums[i] == 0 并且 maxTemp 不大于 i 的话, 说明达到不了后面了
		if maxTep <= i && nums[i] == 0 {
			return false
		}
		nums[i] = i + nums[i]
		maxTep = max2(maxTep, nums[i])
		// maxTep >= n-1, 说明已经能跳到最后了
		if maxTep >= n-1 {
			return true
		}
	}
	return true
}

func max2(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
