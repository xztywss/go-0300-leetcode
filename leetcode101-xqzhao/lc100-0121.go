package leetcode101_xqzhao

// TODO 买卖股票的最佳时机 【-】 脑筋急转弯

func maxProfit(prices []int) (s int) {
	minBuy := prices[0]
	maxPro := 0

	for i := 0; i < len(prices); i++ {
		maxPro = max5(maxPro, prices[i]-minBuy)
		minBuy = min5(minBuy, prices[i])
	}

	return maxPro
}

func max5(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func min5(a int, b int) int {
	if a > b {
		return b
	}
	return a
}
