package leetcode101_xqzhao

// TODO 【没思路】 括号生成 【-】 递归

func generateParenthesis(n int) []string {
	res := []string{}

	var dfs022 func(lRemain int, rRemain int, tmp string)
	dfs022 = func(lRemain int, rRemain int, tmp string) {
		if len(tmp) == 2*n {
			res = append(res, tmp)
			return
		}
		if lRemain > 0 {
			dfs022(lRemain-1, rRemain, tmp+"(")
		}
		if lRemain < rRemain {
			dfs022(lRemain, rRemain-1, tmp+")")
		}
	}
	dfs022(n, n, "")

	return res
}
