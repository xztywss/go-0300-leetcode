package leetcode101_xqzhao

// TODO 验证二叉搜索树 【-】 中序遍历 | 脑筋急转弯

import "math"

// TODO 验证二叉搜索树

func isValidBST(root *TreeNode) bool {
	sign := true // 用于保存中序遍历中上一个节点的值
	prev := math.MinInt64

	// 定义递归函数
	var dfs098 func(node *TreeNode)
	dfs098 = func(node *TreeNode) {
		// 递归终止条件
		if node == nil {
			return
		}

		// 保存结果 + 继续递归
		dfs098(node.Left)
		if node.Val <= prev {
			sign = false
		}
		prev = node.Val
		dfs098(node.Right)
	}

	dfs098(root)

	return sign
}
