package leetcode101_xqzhao

// TODO 环形链表 【-】 单链表 | 脑筋急转弯

func hasCycle(head *ListNode) bool {
	slow, fast := head, head
	for fast != nil {
		slow = slow.Next
		// fast.Next == nil 则直接返回, 继续执行会报空指针异常
		if fast.Next == nil {
			return false
		}
		fast = fast.Next.Next
		if slow == fast {
			return true
		}
	}
	return false
}
