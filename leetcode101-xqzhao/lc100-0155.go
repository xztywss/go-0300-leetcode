package leetcode101_xqzhao

// TODO 【没思路】 实现一个栈 【-】 栈

type MinStack struct {
	Istack []int
	IMin   []int // 保存到当前位置的最小值, Imin 一定是非递增的
}

func Constructor2() MinStack {
	mStack := MinStack{
		Istack: make([]int, 0),
		IMin:   make([]int, 0),
	}
	return mStack
}

func (p *MinStack) Push(val int) {
	p.Istack = append(p.Istack, val)
	if len(p.IMin) == 0 || val <= p.IMin[len(p.IMin)-1] {
		p.IMin = append(p.IMin, val)
		return
	}
	p.IMin = append(p.IMin, p.IMin[len(p.IMin)-1]) // 如果大于最小值, 这把最小值添加进最小值队列占位
}

func (p *MinStack) Pop() {
	p.Istack = p.Istack[:len(p.Istack)-1]
	p.IMin = p.IMin[:len(p.IMin)-1]
}

func (p *MinStack) Top() int {
	return p.Istack[len(p.Istack)-1]
}

func (p *MinStack) GetMin() int {
	return p.IMin[len(p.IMin)-1]
}
