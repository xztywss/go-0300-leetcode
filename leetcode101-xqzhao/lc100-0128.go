package leetcode101_xqzhao

// TODO 【没思路】 最长连续序列 【-】 哈希表 | 脑筋急转弯

func longestConsecutive(nums []int) (ans int) {
	m := make(map[int]bool, 0)

	for _, v := range nums {
		m[v] = true
	}

	for v := range m {
		if m[v-1] { // n-1在哈希表中存在，则该点不是连续序列的起点
			continue
		}
		cnt := 1 // 以 n 做起点的连续序列的数量
		for m[v+cnt] {
			cnt++
		}
		ans = max6(ans, cnt)
	}
	return
}

func max6(a, b int) int {
	if a > b {
		return a
	}
	return b
}
