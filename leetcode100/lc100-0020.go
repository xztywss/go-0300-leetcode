package leetcode100

// TODO 栈 [经典] 【-】 有效的括号
// TODO https://leetcode.cn/problems/valid-parentheses/solution/by-wu-ji-yan-a0-n5g9/

func isValid(s string) bool {
    if len(s)%2 == 1 {
        return false
    }
    // 入栈出栈，设置字典
    zid := map[byte]byte{
		')': '(',
		']': '[',
		'}': '{',
    }
    stack := []byte{} // 空切片就是栈
    for i := 0; i < len(s); i++ {
        if zid[s[i]]>0 {    // 判断字典中是否存在这个输入的字符，判断是否是右括号
            if len(stack) == 0 || stack[len(stack)-1] != zid[s[i]] {
                return false
            }  // 输入是右括号，如果栈这时候是空或者是左不匹配右，则不满足条件
            stack = stack[:len(stack)-1]     // 如果匹配了，则栈顶元素删除
        } else {
            stack = append(stack, s[i]) // 输入是左括号，入栈
        }
    }
    return len(stack) == 0 //len(stack) == 0为真就是true，否则就是false
}