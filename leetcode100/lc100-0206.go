package leetcode100

// TODO 链表 [经典] 【-】 反转链表
// TODO https://leetcode.cn/problems/reverse-linked-list/solution/by-guowei01-636f/

func reverseList(head *ListNode) *ListNode {
	var last *ListNode
	curr := head
	for curr != nil {
		// 保证 head 能够正常被遍历的临时变量
		next := curr.Next
		// 指向前一个节点，完成题目要求的反转操作
		curr.Next = last
		// last 向后移动
		last = curr
		// head 向后移动
		curr = next
	}
	return last
}
