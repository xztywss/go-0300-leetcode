package leetcode100

// TODO 栈 | 实现一个栈 [经典] 【-】 最小栈
// TODO https://leetcode.cn/problems/min-stack/solution/xiang-xi-tong-su-de-si-lu-fen-xi-duo-jie-fa-by-38/

type MinStack struct {
	Min, Arr []int
}

func Constructor2() MinStack {
	return MinStack{}
}

func (p *MinStack) Push(val int) {
	p.Arr = append(p.Arr, val)
	if len(p.Min) == 0 || val < p.Min[len(p.Min)-1] {
		p.Min = append(p.Min, val)
		return
	}
	p.Min = append(p.Min, p.Min[len(p.Min)-1])
}

func (p *MinStack) Pop() {
	p.Arr = p.Arr[:len(p.Arr)-1]
	p.Min = p.Min[:len(p.Min)-1]
}

func (p *MinStack) Top() int {
	return p.Arr[len(p.Arr)-1]
}

func (p *MinStack) GetMin() int {
	return p.Min[len(p.Min)-1]
}
