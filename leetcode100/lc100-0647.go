package leetcode100

// TODO 中心扩散法 【-】 回文子串
// TODO https://leetcode.cn/problems/palindromic-substrings/solution/by-song-jia-liang-p4l9/

func countSubstrings(s string) int {
	ans := 0

	for i := 0; i < len(s); i++ {
		ans += extend(s, i, i, len(s))
		ans += extend(s, i, i+1, len(s))
	}
	return ans
}

func extend(s string, i, j int, n int) int {
	ans := 0
	for i >= 0 && j < n && s[i] == s[j] {
		ans++
		i--
		j++
	}
	return ans
}
