package leetcode100

// TODO 位运算 | 异或 【-】 只出现一次的数字
// TODO leetcode.cn/problems/single-number/solution/zhi-chu-xian-yi-ci-de-shu-zi-by-leetcode-solution/

func singleNumber(nums []int) int {
	single := 0

	for _, num := range nums {
		single = single ^ num
	}

	return single
}
