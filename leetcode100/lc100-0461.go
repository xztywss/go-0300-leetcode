package leetcode100

// TODO 位运算 | 异或 | 右移  [经典] 【-】 汉明距离
// TODO https://leetcode.cn/problems/hamming-distance/solution/goshi-yong-yi-huo-jie-jue-yi-ming-ju-chi-_chao-xia/

func hammingDistance(x int, y int) int {
	i := x ^ y //x和y异或,得到一个新的数(异或相同为0,不同为1,此时咱们需要统计1的个数)
	count := 0 //定义数量的初始值为0

	for i != 0 { //只要i不为0,那就继续循环
		if (i & 1) == 1 { //如果i和1相与,值为1的话就count++
			count++
		}
		i = i >> 1 //i右移一位
	}

	return count
}
