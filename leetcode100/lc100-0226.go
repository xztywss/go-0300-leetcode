package leetcode100

// TODO 递归 【-】 翻转二叉树
// TODO https://leetcode.cn/problems/invert-binary-tree/solution/shou-hua-tu-jie-san-chong-xie-fa-di-gui-liang-chon/

func invertTree(root *TreeNode) *TreeNode {
	if root == nil {
		return root
	}
	
	invertTree(root.Left)
	invertTree(root.Right)

	root.Left, root.Right = root.Right, root.Left

	return root
}
