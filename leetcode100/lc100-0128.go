package leetcode100

// TODO 哈希表 【-】 最长连续序列
// TODO https://leetcode.cn/problems/longest-consecutive-sequence/solution/by-wwsw-xnjg/

func longestConsecutive(nums []int) (ans int) {
	set := map[int]bool{}
	for _, n := range nums {
		set[n] = true
	}
	for n := range set {
		if set[n-1] { // n-1在哈希表中存在，则该点不是连续序列的起点
			continue
		}
		cnt := 1 // 以 n 做起点的连续序列的数量
		for set[n+1] {
			n++
			cnt++
		}
		ans = max5(ans, cnt)
	}
	return
}
func max5(a, b int) int {
	if a > b {
		return a
	}
	return b
}
