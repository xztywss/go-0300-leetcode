package leetcode100

// TODO 脑筋急转弯 | 快慢指针 【-】 环形链表
// TODO https://leetcode.cn/problems/linked-list-cycle/solution/huan-xing-lian-biao-by-leetcode-solution/

func hasCycle(head *ListNode) bool {
    if head == nil || head.Next == nil {
        return false
    }
    slow, fast := head, head.Next   // 慢指针、快指针
    for fast != slow {              // 如果相遇，则退出循环
        if fast == nil || fast.Next == nil {
            return false
        }
        slow = slow.Next
        fast = fast.Next.Next
    }
    return true
}