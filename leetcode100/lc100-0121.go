package leetcode100

// TODO 脑筋急转弯 | 一次遍历 【-】 买卖股票的最佳时机
// TODO https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/solution/-by-gracious-vvilson1bb-w8yb/

import "math"

func maxProfit(prices []int) (s int) {
	maxs := math.MinInt64
	mins := math.MaxInt64
	for i := 0; i < len(prices); i++ {
		// 记录最小值
		if prices[i] < mins {
			mins = prices[i]
		}
		// 记录最大最大差值
		maxs = max4(maxs, prices[i]-mins)

	}
	//返回最大差值
	return maxs
}

func max4(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}
