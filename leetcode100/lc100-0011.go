package leetcode100

// TODO 脑筋急转弯 | 数组双指针 【-】 盛最多水的容器
// TODO https://leetcode.cn/problems/container-with-most-water/solution/shuang-zhi-zhen-jie-fa-xiang-qing-jian-d-2diq/

func maxArea(height []int) int {
	//初始化默认面积、双指针初始下标
	maxArea := 0
	left, right := 0, len(height)-1

	// for循环
	for left < right {
		// area ＝ width * height  木桶的最大容量取决于最短的那根木板和宽度宽度 (j - i)
		area := min(height[left], height[right]) * (right - left)
		// 更新最大面积
		if area > maxArea {
			maxArea = area
		}
		if height[left] > height[right] {
			right--
		} else {
			left++
		}
	}

	return maxArea
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
