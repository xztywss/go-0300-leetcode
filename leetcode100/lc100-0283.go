package leetcode100

// TODO 双指针 [经典] 【-】 移动零
// TODO https://leetcode.cn/problems/move-zeroes/solution/yi-dong-ling-by-leetcode-solution/

func moveZeroes(nums []int) {
	left, right := 0, 0
	n := len(nums)

	for right < n {
		if nums[right] != 0 {
			nums[left], nums[right] = nums[right], nums[left]
			left++
		}
		right++
	}
}
