package leetcode100

// TODO 脑筋急转弯 【-】 除自身以外数组的乘积
// TODO https://leetcode.cn/problems/product-of-array-except-self/solution/liang-ci-bian-li-yi-ci-bian-li-goyu-yan-nhbii/

func productExceptSelf(nums []int) []int {
	n := len(nums)
	res := make([]int, n)
	for i := 0; i < n; i++ {
		res[i] = 1
	}
	left, right := 1, 1
	for i := 0; i < n; i++ {
		res[i] = left
		left *= nums[i]
	}
	for i := n - 1; i >= 0; i-- {
		res[i] *= right
		right *= nums[i]
	}
	return res
}
