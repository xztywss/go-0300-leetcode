package leetcode100

// TODO 递归 [经典] 【-】 对称二叉树
// TODO https://leetcode.cn/problems/symmetric-tree/solution/dui-cheng-er-cha-shu-by-leetcode-solution/

func isSymmetric(root *TreeNode) bool {
    return check(root, root)
}

func check(p, q *TreeNode) bool {
    // p、q 都为空, 则 p、q对称
    if p == nil && q == nil {
        return true
    }
    // p、q 只有一个为空, 则 p、q不对称
    if p == nil && q!=nil || q == nil && p != nil {
        return false
    }
    return p.Val == q.Val && check(p.Left, q.Right) && check(p.Right, q.Left) 
}