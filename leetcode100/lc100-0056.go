package leetcode100

// TODO 脑筋急转弯 | 排序+双指针 【-】 合并区间
// TODO https://leetcode.cn/problems/merge-intervals/solution/shou-hua-tu-jie-56he-bing-qu-jian-by-xiao_ben_zhu/

import "sort"

func merge(intervals [][]int) [][]int {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	res := [][]int{}
	prev := intervals[0]

	for i := 1; i < len(intervals); i++ {
		cur := intervals[i]
		if prev[1] < cur[0] { // 没有一点重合
			res = append(res, prev)
			prev = cur
		} else { // 有重合
			prev[1] = max9(prev[1], cur[1])
		}
	}
	res = append(res, prev)
	return res
}

func max9(a, b int) int {
	if a > b {
		return a
	}
	return b
}
