package leetcode100

// TODO 哈希表 [经典] 【-】 两数之和
// TODO https://leetcode.cn/problems/two-sum/solution/liang-shu-zhi-he-by-leetcode-solution/

func twoSum(nums []int, target int) []int {
	m2 := make(map[int]int)

	len := len(nums)
	for i := 0; i < len; i++ {
		if v, ok := m2[target-nums[i]]; ok {
			return []int{i, v}
		}
		m2[nums[i]] = i
	}

	return []int{}
}
