package leetcode100

// TODO 动态规划 | 状态转移方程 [经典] 【-】 打家劫舍
// TODO https://leetcode.cn/problems/house-robber/solution/da-jia-jie-she-by-leetcode-solution/
// TODO 状态转移方程: dp[i]=max(dp[i−2]+nums[i],dp[i−1])

func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	dp := make([]int, len(nums))
	dp[0] = nums[0]
	dp[1] = max7(nums[0], nums[1])
	for i := 2; i < len(nums); i++ {
		dp[i] = max7(dp[i-2]+nums[i], dp[i-1])
	}
	return dp[len(nums)-1]
}

func max7(x, y int) int {
	if x > y {
		return x
	}
	return y
}
