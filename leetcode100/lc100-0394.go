package leetcode100

// TODO 栈 | 字符串处理 [经典]【-】 字符串解码
// TODO https://leetcode.cn/problems/decode-string/solution/zhan-de-ji-yi-nei-ceng-de-jie-ma-liao-bie-wang-lia/

import (
	"strconv"
	"strings"
)

func decodeString(s string) string {
	numStack := []int{}      // 存倍数的栈
	strStack := []string{}   // 存 待拼接的str 的栈
	num := 0                 // 倍数的“搬运工”
	result := ""             // 字符串的“搬运工”
	for _, char := range s { // 逐字符扫描
		if char >= '0' && char <= '9' { // 遇到数字
			n, _ := strconv.Atoi(string(char))
			num = num*10 + n // 算出倍数
		} else if char == '[' { // 遇到 [
			strStack = append(strStack, result) // result串入栈
			result = ""                         // 入栈后清零
			numStack = append(numStack, num)    // 倍数num进入栈等待
			num = 0                             // 入栈后清零
		} else if char == ']' { // 遇到 ]，两个栈的栈顶出栈
			count := numStack[len(numStack)-1] // 获取拷贝次数
			numStack = numStack[:len(numStack)-1]
			str := strStack[len(strStack)-1]
			strStack = strStack[:len(strStack)-1]
			result = string(str) + strings.Repeat(result, count) // 构建子串
		} else {
			result += string(char) // 遇到字母，追加给result串
		}
	}
	return result
}
