package leetcode100

// TODO 脑筋急转弯 | 原地置换 【-】 找到所有数组中消失的数字
// TODO https://leetcode.cn/problems/find-all-numbers-disappeared-in-an-array/solution/shou-hua-tu-jie-jiao-huan-shu-zi-zai-ci-kzicg/

func findDisappearedNumbers(nums []int) []int {
	i := 0
	for i < len(nums) {
		if nums[i] == i+1 { // 当前元素出现在它该出现的位置，无需交换
			i++
			continue
		}
		idealIdx := nums[i] - 1        // idealIdx：当前元素应该出现的位置
		if nums[i] == nums[idealIdx] { // 当前元素=它理应出现的位置上的现有元素，说明重复了
			i++
			continue
		}
		nums[idealIdx], nums[i] = nums[i], nums[idealIdx] // 不重复，进行交换
		// 这里不要i++，因为交换过来的数字本身也需要考察，需要交换到合适的位置上
		// 如果 i++ 就会跳过它，少考察了它
	}

	res := []int{}
	for i := 0; i < len(nums); i++ {
		if nums[i] != i+1 { // 值与索引 不对应
			res = append(res, i+1)
		}
	}
	return res
}
