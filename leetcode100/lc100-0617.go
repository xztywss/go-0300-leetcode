package leetcode100

// TODO 递归 【-】 合并二叉树
// TODO https://leetcode.cn/problems/merge-two-binary-trees/solution/shou-hua-tu-jie-by-xiao_ben_zhu-3/

func mergeTrees(t1 *TreeNode, t2 *TreeNode) *TreeNode {
	if t1 == nil && t2 != nil {
		return t2
	}
	if (t1 != nil && t2 == nil) || (t1 == nil && t2 == nil) {
		return t1
	}
	t1.Val += t2.Val
	t1.Left = mergeTrees(t1.Left, t2.Left)
	t1.Right = mergeTrees(t1.Right, t2.Right)
	return t1
}
