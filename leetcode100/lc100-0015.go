package leetcode100

// TODO 排序+双指针 [困难] 【-】 三数之和

import "sort"

func threeSum(nums []int) [][]int {
	nLength := len(nums)
	// 先对数组进行 排序
	sort.Ints(nums)
	ans := make([][]int, 0)

	// 枚举 first
	for first := 0; first < nLength; first++ {
		// 需要和上一次枚举的数不相同
		if first > 0 && nums[first] == nums[first-1] {
			continue
		}
		// third 对应的指针初始指向数组的最右端
		third := nLength - 1
		backFirst := -1 * nums[first]

		// 枚举 second
		for second := first + 1; second < nLength; second++ {
			// 需要和上一次枚举的数不相同
			if second > first+1 && nums[second] == nums[second-1] {
				continue
			}
			// 需要保证 second 的指针在 third 的指针的左侧
			for second < third && nums[second]+nums[third] > backFirst {
				third--
			}
			// 如果指针重合，随着 second 后续的增加
			// 就不会有满足 a+b+c=0 并且 b<c 的 c 了，可以退出循环
			if second == third {
				break
			}
			if nums[second]+nums[third] == backFirst {
				ans = append(ans, []int{nums[first], nums[second], nums[third]})
			}
		}
	}
	return ans
}
