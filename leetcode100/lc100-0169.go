package leetcode100

// TODO 脑筋急转弯 | 摩尔投票 【-】 多数元素
// TODO https://leetcode.cn/problems/majority-element/solution/tu-jie-mo-er-tou-piao-fa-python-go-by-jalan/

func majorityElement(nums []int) int {
	major := 0 // 栈里存的是哪个元素
	count := 0 // 栈里的元素有几个

	for _, num := range nums {
		if count == 0 {
			major = num
		}
		if major == num { // 投票人相等, 则票数+1
			count += 1
		} else {
			count -= 1
		}
	}

	return major
}
