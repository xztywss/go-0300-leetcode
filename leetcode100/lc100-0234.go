package leetcode100

// TODO 脑筋急转弯 【-】 回文链表
// TODO https://leetcode.cn/problems/palindrome-linked-list/solution/shou-hua-tu-jie-hui-wen-lian-biao-kuai-man-zhi-zhe/
// TODO 快慢指针: 实现复杂, 略

func isPalindrome(head *ListNode) bool {
	vals := []int{}
	for head != nil {
		vals = append(vals, head.Val)
		head = head.Next
	}
	start, end := 0, len(vals)-1
	for start < end {
		if vals[start] != vals[end] {
			return false
		}
		start++
		end--
	}
	return true
}
