package leetcode100

// TODO 滑动窗口 [经典] 【-】 无重复字符的最长子串
// TODO https://leetcode.cn/problems/longest-substring-without-repeating-characters/solution/wu-zhong-fu-zi-fu-by-bananajin-6-fxvb/

func lengthOfLongestSubstring(s string) int {
    start, tmp, max := 0, 0, 0
    for i := 0; i < len(s); i++ {
        tmp = i - start + 1
        for j := start; j < i; j++ {
            if s[i] == s[j] {
                tmp = i - start
                start = j + 1
                break
            } 
        }
        if tmp > max {
            max = tmp
        }
    }
    return max
}