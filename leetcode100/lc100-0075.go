package leetcode100

// TODO 双指针 [经典] 【-】 颜色分类
// TODO https://leetcode.cn/problems/sort-colors/solution/shuang-zhi-zhen-fang-fa-goyu-yan-shi-xia-9u2z/

func sortColors(nums []int) {
	left, right := 0, len(nums)-1
	for i := 0; i <= right; {
		if nums[i] == 0 {
			nums[left], nums[i] = nums[i], nums[left]
			left++
			i++
		} else if nums[i] == 1 {
			i++
		} else if nums[i] == 2 {
			nums[i], nums[right] = nums[right], nums[i]
			right--
		}
	}
}
